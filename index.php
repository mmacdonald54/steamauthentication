<?php
require 'steamauth/steamauth.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Wilde Design</title>
    
        <!--Bootstrap Links (and JQuery)-->
        <link rel="stylesheet" href="./Sources/Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./Sources/Bootstrap/css/bootstrap.css">
        <script src="./Sources/JQuery/jquery-2.2.0.min.js"></script>
        <script src="./Sources/Bootstrap/js/bootstrap.min.js"></script>
        <script src="./Sources/Bootstrap/js/bootstrap.js"></script>
    
        <!-- Scripts -->
        <script src="./Scripts/project.js"></script>
    
        <!--Stylesheets-->
        <link rel="stylesheet" href="./Style/project.css">
    
        <!--Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container" style="padding:0;">

            <div class="col-xs-12 text-center" style="margin-top:10px;">
                <?php
                if(!isset($_SESSION['steamid'])) {
                    echo    '<div class="col-xs-12 text-center" id="splash">
                               <img src="/Content/Logo.png" class="img-responsive"/>
                            </div>'; 
                    //echo the login button
                    echo ("<a href='?login'><img id='btnLogin' src='http://cdn.steamcommunity.com/public/images/signinthroughsteam/sits_02.png'></a>");

                }  else {
                    include ('steamauth/userInfo.php'); //To access results from Steam
                    switch($steamprofile['personastate']){
                        case 1:
                            $online = "<h4>You are currently Online! <span class='glyphicon glyphicon-off' style='color:#00a300'></span></h4>";
                            break;
                        case 2:
                            $online = "<h4>You are currently Away! <span class='glyphicon glyphicon-off' style='color:#c1b700'></span></h4>";
                            break;
                        case 0:
                            $online = "<h4>You are currently Offline! <span class='glyphicon glyphicon-off' style='color:#ba4140'></span></h4>";
                            break;
                        default:
                            $online = "<span class='glyphicon glyphicon-off' style='color:black'></span>";
                    }
                    //add the profile name
                    $doc = "<div class='col-xs-12'><h1>Welcome, ".$steamprofile['personaname']."!</h1></div>";

                    //add the avatar and online status
                    $doc .= "<div class='col-xs-12 col-md-4 col-md-offset-4'><img class='img-circle' src='".$steamprofile['avatarmedium']."'/>".$online."</div>";

                    //add the logout button
                    $doc .= "<a href='/Discord/discord.php'><button id='btnLogout' class='btn btn-primary' name='logout' type='submit'>Disconnect Steam</button></a>";


                    //echo the doc
                    echo $doc;
                }
                ?>
            </div>
        </div>
    </body>
</html>
