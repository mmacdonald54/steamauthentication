<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'TrafficCophp' => array($vendorDir . '/TrafficCophp/ByteBuffer/src'),
    'SecurityLib' => array($vendorDir . '/ircmaxell/security-lib/lib'),
    'React\\Partial' => array($vendorDir . '/react/partial/src'),
    'RandomLib' => array($vendorDir . '/ircmaxell/random-lib/lib'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
);
