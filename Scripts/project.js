$(window).on("load", function(){
    $("#prlLogo").fadeIn('slow');
    $(".brandName").each(function(index){
        $(this).delay(500*(index+1)).fadeIn('slow');
    });
    $("#infoPanel").fadeIn('slow');
    centerVert();
    $(window).on("resize", function(){
        centerVert();
    })

});

function centerVert(){
    //Logo
    var $div = $(".centerVert");
    var $winHeight = $(document).height();
    var $divHeight = $($div).height();
    var $marginTop = ($winHeight - $divHeight - 60)/2;
    $div.css("margin-top", $marginTop);
}