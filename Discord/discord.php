<?php
    include  '../vendor/autoload.php';
    $provider = new \Discord\OAuth\Discord([
        'clientId'     => '214589996789661696',
        'clientSecret' => 'eRcNApFR_YSmSYanPK_AMiePt4X8eFIT',
        'redirectUri'  => 'http://wilde.design/Discord/discord.php',
    ]);

    if (! isset($_GET['code']) && ! isset($_COOKIE['id'])) {
        echo    '<a id="btnDiscord" href="'.$provider->getAuthorizationUrl().'" class="btn btn-warning" 
                    data-toggle="tooltip" data-placement="bottom" data-trigger="manual" title="Sign in with Discord!">                    
                    <img src="http://www.discordapp.com/assets/192cb9459cbc0f9e73e2591b700f1857.svg" height="25"/>
                </a>';
    } elseif(isset($_GET['code'])) {
        $token = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code'],
        ]);

        // Get the user object.
        $user = $provider->getResourceOwner($token);

        //create ID var and store in cookie (this would change to storing in a DB...)
        $id = $user->id;
        setcookie('id', $id, time() + (86400 * 30), '/');

        //create username var and store in cookie (this would change to storing in a DB...)
        $username = $user->username;
        setcookie('username', $username, time() + (86400 * 30), '/');

        //create email var and store in cookie (this would change to storing in a DB...)
        $email = $user->email;
        setcookie('email', $email, time() + (86400 * 30), '/');

        //create discriminator (Discord User ID) var and store in cookie (this would change to storing in a DB...)
        $discriminator = $user->discriminator;
        setcookie('discriminator', $discriminator, time() + (86400 * 30), '/');

        $avatar = $user->avatar;
        setcookie('avatar', $avatar, time() + (86400 *30), '/');

        header('Location: userProfile.php');
    }
    else{
        header('Location: userProfile.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Discord</title>

        <!--Bootstrap Links (and JQuery)-->
        <link rel="stylesheet" href="../Sources/Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../Sources/Bootstrap/css/bootstrap.css">
        <script src="../Sources/JQuery/jquery-2.2.0.min.js"></script>
        <script src="../Sources/Bootstrap/js/bootstrap.min.js"></script>
        <script src="../Sources/Bootstrap/js/bootstrap.js"></script>

        <!-- Scripts -->
        <script src="../Scripts/project.js"></script>

        <!--Stylesheets-->
        <link rel="stylesheet" href="../Style/project.css">
        <link href="https://fonts.googleapis.com/css?family=Heebo:100" rel="stylesheet">

        <!--Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
    </head>
    <body class="background">
    <nav id="mainNav" class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" style="padding:10px" href="/Discord/discord.php"><img height="40" src="http://www.prorl.com/logos/1466457638-FIXEEEEEEED.png"/></a>
            </div>
        </div>
    </nav>
        <div class="container-fluid">
            <div id="slideIn" class="centerVert col-xs-12 col-md-4 col-md-offset-7">
                <div class="text-center" style="min-height:411px;">
                    <img style="display:none;" id="prlLogo" src="/Content/PRLlogo.png" height="150"/>
                    <h2 style="display:none;" class="brandName">Pro</h2>
                    <h2 style="display:none;" class="brandName">Rivalry</h2>
                    <h2 style="display:none;" class="brandName">League</h2>
                </div>
            </div>
        </div>
    </body>
    <script> //This needs to be at the bottom
        $(document).ready(function(){
            setTimeout(function(){
                $("[data-toggle='tooltip']").tooltip('show');
                    setTimeout(function(){
                        $("[data-toggle='tooltip']").tooltip('hide');
                    }, 2000);
            }, 2000);


        });
    </script>
</html>
