<?php
require '../steamauth/steamauth.php';
if(!isset ($_COOKIE['id'])){
    //If the user isn't signed in, then redirect to the login screen
    header('Location: /Discord/discord.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Discord</title>

    <!--Bootstrap Links (and JQuery)-->
    <link rel="stylesheet" href="../Sources/Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Sources/Bootstrap/css/bootstrap.css">
    <script src="../Sources/JQuery/jquery-2.2.0.min.js"></script>
    <script src="../Sources/Bootstrap/js/bootstrap.min.js"></script>
    <script src="../Sources/Bootstrap/js/bootstrap.js"></script>

    <!-- Scripts -->
    <script src="../Scripts/project.js"></script>

    <!--Stylesheets-->
    <link rel="stylesheet" href="../Style/project.css">

    <!--Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
</head>
<body class="background">

<nav id="mainNav" class="navbar navbar-default" style="border-radius:0">
    <div class="container-fluid">
        <div class="navbar-header"> 
            <a class="navbar-brand" style="padding:10px" href="/Discord/discord.php"><img height="40" src="http://www.prorl.com/logos/1466457638-FIXEEEEEEED.png"/></a>
        </div>
    </div>
</nav>
<a id="btnLogout" href="logout.php" class="btn btn-warning"><img src="../Content/DiscordSmall.png" height="25"/>Logout</a>
<div class="container">
    <div id="infoPanel" class="centerVert col-xs-12 col-md-4 col-md-offset-7" style="min-height:411px; display:none;">
        <div class="panel panel-default">
            <div class="panel-heading text-center">Welcome, <?php echo($_COOKIE['username']);?>!</div>
            <div class="panel-body">
                <div class="text-center">
                    <img class="img-circle" src="https://cdn.discordapp.com/avatars/<?php echo($_COOKIE['id']);?>/<?php echo($_COOKIE['avatar']);?>.jpg"
                </div>
                <hr/>
                <ul class="list-group">
                    <li class="list-group-item">Email: <?php echo($_COOKIE['email']);?></li>
                    <li class="list-group-item">Discord ID: <?php echo($_COOKIE['discriminator']);?></li>
                    <li class="list-group-item">
                        <?php
                        if(!isset($_SESSION['steamid'])) {
                            echo('
                                <a href = "?login" class="btn btn-danger form-control" >
                                    <img src = "/Content/SteamLogo.png" height = "25px" />&nbsp;Steam Not Verified
                                </a >
                            ');
                        }
                        else{
                            include ('../steamauth/userInfo.php'); //To access results from Steam
                            echo('
                                <a href = "../index.php" class="btn btn-success form-control" >
                                    <img src = "/Content/SteamLogo.png" height = "25px" />&nbsp;Steam Verified
                                </a >
                            ');
                            //save steam username
                            setcookie("steamUser", $steamprofile['personaname'], time() + (86400 * 30), '/');

                            //save steam avatar
                            setcookie("steamAvatar", $steamprofile['avatarmedium'], time() + (86400 * 30), '/');

                            //save steam ID
                            setcookie("steamID", $steamprofile['steamid'], time() + (86400 * 30), '/');

                            //save steam profile state
                            setcookie("steamState", $steamprofile['personastate'], time() + (86400 * 30), '/');
                        }
                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
