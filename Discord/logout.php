<?php
//Remove all cookies...
if (isset($_COOKIE['id'])) {
    setcookie('id', '', time() - 3600, '/');
}
if (isset($_COOKIE['username'])) {
    setcookie('username', '', time() - 3600, '/');
}
if (isset($_COOKIE['email'])) {
    setcookie('email', '', time() - 3600, '/');
}
if (isset($_COOKIE['discriminator'])) {
    setcookie('discriminator', '', time() - 3600, '/');
}
if (isset($_COOKIE['avatar'])){
    setcookie('avatar', '', time() -3600, '/');
}

header('Location: /Discord/discord.php');
